import { Injectable } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  constructor() { }

  webSocketEndPoint: string = 'http://localhost:8080/ws';
    topic: string = "/status/instance-health";
    stompClient: any;


    connect(success, error) {
        console.log("Initialize WebSocket Connection");
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function (frame) {
            _this.stompClient.subscribe(_this.topic, function (sdkEvent) {
                success(_this.onMessageReceived(sdkEvent));
            });
        }, error());
    };

    disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    onMessageReceived(message) {
        console.log("Message Recieved from Server :: " + message);
        return message.body;
    }
}
