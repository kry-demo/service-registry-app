import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceInstance } from './service-instance';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceInstanceService {
  

  serviceInstancesUrl:string = "http://localhost:8080/service-registry/instance/"

  constructor(
    private http: HttpClient) { }

  getServiceInstances(): Observable<ServiceInstance[]> {

    console.log("Loading Services")

    return this.http.get<ServiceInstance[]>(this.serviceInstancesUrl);
  }

  addServiceInstance(serviceInstance: ServiceInstance):Observable<ServiceInstance> {
    return this.http.post<ServiceInstance>(this.serviceInstancesUrl, serviceInstance);
  }

  private handleError<T>(operation = 'operation', result?: T) {

    console.log("Error http" + result);

    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.log("Error" + error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
