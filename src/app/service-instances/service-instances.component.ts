import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServiceInstanceService} from '../service-instance.service';
import {ServiceInstance, ServiceInstanceHealth, ApiError} from '../service-instance';
import {WebsocketService} from '../websocket.service';

@Component({
  selector: 'app-service-instances',
  templateUrl: './service-instances.component.html',
  styleUrls: ['./service-instances.component.css']
})
export class ServiceInstancesComponent implements OnInit {

  serviceInstances: ServiceInstance[] = [];
  serviceInstance: ServiceInstance;
  statusMessage: ApiError;

  constructor(private serviceInstanceService: ServiceInstanceService,
              private http: HttpClient, private websocketService: WebsocketService) {
  }

  ngOnInit(): void {

    this.setDefaultServiceInstance();

    console.log("Loading Service Instances");
    this.getServiceInstances();
    this.websocketService.connect(message => {
      console.log("Websocket message received: " + JSON.stringify(message));
      let serviceInstanceHealth: ServiceInstanceHealth = JSON.parse(message);
      this.serviceInstances
        .find(serviceInstance => serviceInstance.id == serviceInstanceHealth.instanceId)
        .serviceStatus = serviceInstanceHealth.serviceStatus;
    }, error => {
      console.log("Websocket error: " + JSON.stringify(error));
    });
  }

  setDefaultServiceInstance(): void {
    this.serviceInstance = {
      id: 0,
      name: "",
      endpoint: "",
      dateCreated: "",
      serviceStatus: "UNKNOWN"
    };
  }

  getServiceInstances(): void {
    this.serviceInstanceService.getServiceInstances()
      .subscribe(serviceInstances => {
        this.serviceInstances = serviceInstances;
        console.log("Service instances received:" + serviceInstances)
      });
  }

  addServiceInstance(): void {
    console.log("Adding new Service Instance" + JSON.stringify(this.serviceInstance));

    this.serviceInstanceService.addServiceInstance(this.serviceInstance)
      .subscribe(serviceInstance => {
          this.serviceInstances.push(serviceInstance);
          console.log("Service instances received:" + serviceInstance);
          this.statusMessage = {
            errorCode: "0",
            message: "Success!"
          };
          this.setDefaultServiceInstance();
        },
        err => {
          console.log("Error occured [HTTP-" + err.status + "]" + JSON.stringify(err));
          let apiError = err.error;
          if (apiError.errorCode && apiError.message) {
            this.statusMessage = {
              errorCode: apiError.errorCode,
              message: apiError.message
            };
          } else {
            this.statusMessage = {
              errorCode: "999",
              message: "Unknown error occured!"
            };
          }
          this.setDefaultServiceInstance();
        });
  }

  disconnect() {
    this.websocketService.disconnect();
  }
}
