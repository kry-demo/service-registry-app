import { TestBed } from '@angular/core/testing';

import { ServiceInstanceService } from './service-instance.service';

describe('ServiceInstanceService', () => {
  let service: ServiceInstanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceInstanceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
