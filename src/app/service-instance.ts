export interface ServiceInstance {
    id: number;
    name: string;
    endpoint:string;
    dateCreated: string;
    serviceStatus:string
}

export interface ServiceInstanceHealth{
  instanceId: number;
  serviceStatus: string;
}

export interface ApiError{
    errorCode:string;
    message:string;
}
