# ServiceGuardian

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2.  
To run this project you need to have following tolls:  
1. NodeJs - Refer to instructions in [Angular setup page](https://angular.io/guide/setup-local)
2. Git Client

#### Clone the project to your local machine

To clone the project from bitbucket, use one the following commands

`` git clone git@bitbucket.org:kry-demo/service-registry-app.git``
  
`` git clone https://Namil@bitbucket.org/kry-demo/service-registry-app.git``  

#### Build angular app  
Go to sources root.  

`` cd service-registry-app/``  

Trigger npm to build the project  

`` npm install``  

#### Run Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. 

> Note: TO successfuly run the app, you need to have Service-Registry backend application up and running (https://bitbucket.org/kry-demo/service-registry/src/develop/)
